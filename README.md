Apple II Disassemblies
======================

Listings from [`6502disassembly.com` Apple II SourceGen Disassembly
Projects][6dsg], converted from HTML to text and possibly with local
modifications:
- [`a2auto.dis`](a2auto.dis) ([source][6dsg_autoF8]): Autostart ROM, 1978.
- [`a2mon.dis`](a2mon.dis) ([source][6dsg_origF8]): Original ROM, 1977.

Other:
- [`appleiimonitor.s`](appleiimonitor.s) ([source][jtmonport]): Jeff
  Tranter's port of the Apple II monitor to the Apple I. Useful for
  his comments.
- [`aplogo/`](aplogo/): Some early versions of MIT Apple Logo from the
  [`files/aplogo`][its/aplogo] subdir of [PDP-10/its-vault][its].


Support Tools
-------------

The `tool/` subdirectory contains Git submodules for several tools used to
reverse-engineer this code. Note that these are mostly Linux programs, and
some of the repos require a case-sensitive file system to avoid filename
conflicts. Fetch the submodules with `git submodule update --init`.

#### dis6502

`dis6502` requires the following Debian packages to build:

    apt-get install build-essential flex docutils-common

Typical tweaks to the dis6502 `Makefile` include:
- Change the `dis6502.1` target to run `rst2man` instead of `rst2man.py`
- Change `INSTDIR` to your preferred $PREFIX.

#### dos33fs

The utilities we use are mainly those under `utils/dos33fs-utils/`.



<!-------------------------------------------------------------------->
[6dsg]: https://6502disassembly.com/
[6dsg_autoF8]: https://6502disassembly.com/a2-f8rom/AutoF8ROM.html
[6dsg_origF8]: https://6502disassembly.com/a2-f8rom/OrigF8ROM.html
[its]:  https://github.com/PDP-10/its-vault/
[its/aplogo]: https://github.com/PDP-10/its-vault/tree/master/files/aplogo
[jtmonport]: https://github.com/jefftranter/6502/blob/master/asm/Apple%5D%5BMonitor/appleiimonitor.s
