The Oregon Trail
================

- `sideA.dsk`,`sideB.dsk`: Images from archive.org item [MECC-A157 The
  Oregon Trail v1.4 (4am crack)][14-4am], found in the ZIP file ` MECC-A157
  The Oregon Trail v1.4 (4am crack).zip` in that item. These are DOS 3.3
  (16-sector 6-and-2 GCR) format, but [self-booting][wp-sb] rather than
  using Apple DOS.
- `sideA/`,`sideB/`: Files extracted from disk image.
- `basic/`: De-tokensed (ASCII) BASIC files from `sideA/`.

Archive.org has [a scan of the game manual][manual].

The file extraction, conversion and disassembly was done with the aid of
the tools in [`../tool/`](../tool/). See [`../README.md`](../README.md) for
more information on those.



<!-------------------------------------------------------------------->
[14-4am]: https://archive.org/details/MECC_A157_v14_4amCrack
[manual]: https://archive.org/details/MECC_A157_The_Oregon_Trail_Manual/mode/2up
[wp-sb]: https://en.wikipedia.org/wiki/Self-booting_disk
