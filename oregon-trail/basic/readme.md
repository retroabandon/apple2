Oregon Trail De-Tokenized BASIC Files
=====================================

These are all the Applesoft BASIC programs from `../disk/`,
detokenized with [`applesoft_detokenizer`][ad].

Note that these do _not_ include any additional non-BASIC data in the file,
such as appended machine lanugage (see below).


File/Program Information
------------------------

### HELLO

The tokenized file is actually $7B5 bytes long, of which only the first
~160 bytes is a BASIC program; the remainder is a machine-language program.
This starts at offset $A1 (161) in the file, which is at $8A0 when loaded
into memory. It is run by the BASIC program with  `CALL 2208`.



<!-------------------------------------------------------------------->
[ad]: https://github.com/softwarejanitor/applesoft_detokenizer
