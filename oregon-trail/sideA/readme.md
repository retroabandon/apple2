Oregon Trail Files
==================

The files in this directory were extracted from the disk image using
`dos33` from the [dos33fsprogs] toolkit. You may also find `dos33 DUMP`
useful to get further low-level details about the disk format.


Disk Catalog
-------------

    DISK VOLUME 0

     I 000 [OREGON TRAIL SIDE 1    /0414]
    *A 009 HELLO
    *B 008 OREGON.IMA
    *A 006 TOMB.LIB
    *A 006 BUY.LIB
    *A 018 RIVER.LIB
    *A 006 PACE.LIB
    *A 004 RATION.LIB
    *A 004 PART.LIB
    *A 006 TRADE.LIB
    *A 002 MAP.LIB
    *A 003 TALK.LIB
    *A 005 HUNT.LIB
    *A 005 LF.LIB
    *A 004 CROSS.LIB
    *B 017 L0.PCK
    *B 022 L1.PCK
    *B 017 L2.PCK
    *B 018 L3.PCK
    *B 017 L4.PCK
    *B 014 M0.PCK
    *B 015 TS.PCK
    *B 016 OREGON1.SEQ
     B 003 HISCORE.SEQ
     B 002 TOMB.SEQ
    *B 002 MS0.BIN
    *B 002 MS1.BIN
    *B 002 MS2.BIN
    *B 003 MS3.BIN
    *B 002 MS4.BIN
    *B 019 VAR.BIN
    *B 002 PT.BIN
    *B 002 TS.BIN
    *B 007 PRAIRIE.IMA
    *B 016 FIRST.IMA
    *B 006 EVENTS.IMA
    *B 006 RIVER.IMA
    *A 006 COMMON.LIB
    *B 002 MECC$$DISK
    *A 040 OREGON TRAIL
    *A 024 BUY SUPPLIES
    *A 027 MENU
    *A 011 MANAGEMENT
    *A 003 FLIP.LIB



<!-------------------------------------------------------------------->
[dos33fsprogs]: https://github.com/deater/dos33fsprogs
