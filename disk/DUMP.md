DUMP.ASM Notes
==============

Locations:
- $0002: Track to dump.
- $0874: Dump end address LSB; default $AF (175). Change to $1F to get
  just four lines of dump when the routine exits.
- $1000: Data from track.

Format (see _Beneath Apple DOS_ p. 3-12 for more details):
- `DE AA EB`: Epilogue of address and data fields.
- `D5 AA 96`: Address field, followed by volume, track, sector,
  checksum, epilogue.
- `D5 AA AD`: Data field, followed by 342 bytes GCR data (6 and 2),
  checksum, epilogue.

The volume, track, and sector bytes in the address field are odd-even
encoded: bits 7,5,3,1 are always 1, and the data bits from most to
lead significant are H6 L6 H4 L4 H2 L2 H0 L0.

Here are two examples, done on an Apple IIc. It's not clear why the
epilogues are not always `DE AA EB`, which is what _Beneath Apple DOS_
says they always are.

    prologue   vol   track   sec   cksum  epilogue  sync
    --------  -----  -----  -----  -----  --------  -----------------
    D5 AA 96  FF FE  AA BB  AB AF  FE EA  DE AA EA  B4 F8 FE FF FF ...
               $FE    $11    $07    $EB
    D5 AA 96  FF AA  AA BB  AA AB  FF BA  DE AA E6  96 FF FF FF FF ...
               $AA    $11    $03    $BB
